<?php

namespace Technican\API;

class Route {
	protected $method = NULL;
	protected $path = NULL;
	protected $handler = NULL;
	protected $contentType = NULL;

	function __construct($method, $path, $handler, $contentType = "application\\json") {
		$this->method = $method;
		$this->path = $path;
		$this->handler = $handler;
		$this->contentType = $contentType;
	}

	function getMethod() { return $this->method; }
	function getPath() { return $this->path; }
	function getHandler() { return $this->handler; }
	function getContentType() { return $this->contentType; }
}
