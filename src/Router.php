<?php

namespace Technican\API;

class Router {
	private $routes = array();
	private $request = NULL;

	function __construct() {
		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->request = $_REQUEST['request'];
	}

	function addRoute($method, $path, $handler, $contentType = "application/json") {
		array_push($this->routes, new Route($method, $path, $handler, $contentType));
	}

	function getContentType() {
		$ct = NULL;
		foreach ($this->routes as $key => $value) {
			if ($value->getMethod() == $this->method) {
				// Check path
				$pathRegex = "/".str_replace("/", "\/", $value->getPath())."$/";
				if (preg_match($pathRegex, $this->request, $args)) {
					$ct = $value->getContentType();
				}
			}
		}
		return $ct;
	}

	function getHandler() {
		$handler = NULL;
		foreach ($this->routes as $key => $value) {
			if ($value->getMethod() == $this->method) {
				// Check path
				$startSlash = (substr($value->getPath(),0,1) != '/') ? '\/' : '';
				$pathRegex = "/^".$startSlash.str_replace("/", "\/", $value->getPath())."$/";
				if (preg_match($pathRegex, $this->request, $args)) {
					$handler = $value->getHandler();
					$handler->setArgs($args);
					break;
				}
			}
		}
		return $handler;
	}

	function process(&$status) {
		$handler = $this->getHandler();;

		// Start router
		if ($handler !== NULL) {
			return $handler->process($status);
		} else {
			$status['status'] = '422';
			$status['error'] = 'Route is unknown';
			return false;
		}
	}
}

