<?php

namespace Technican\API\Wrappers;

abstract class ListXML extends ListAbstract implements \Sabre\Xml\XmlSerializable {
	abstract function getNamespace();
}
