<?php

namespace Technican\API\Wrappers;

abstract class ListJSON extends ListAbstract implements \JsonSerializable {
	function JsonSerialize() {
		return [
			"meta" => [
				"page" => $this->page,
				"pages" => $this->pages
			],
			"list" => $this->getList()
		];
	}
}
