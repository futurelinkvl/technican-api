<?php

namespace Technican\API\Wrappers;

use Technican\API\TaoDB;

abstract class ListAbstract {
	protected $page = 1;
	protected $pages = 1;
	private $list = [];
	private $filters = [];

	protected function getList() { return $this->list; }
	public function addToList($item) { array_push($this->list, $item); }
	public function addFilter($name, $filter) { $this->filters[$name] = $filter; }
	public function hasFilter($name) { return isset($this->filters[$name]); }
	public function getFilter($name) { if ($this->hasFilter($name)) return $this->filters[$name]; else return NULL; }

	abstract protected function createQuery(TaoDB $db);
	abstract public function load(TaoDB $db);

	protected function refValues($arr){
		if (strnatcmp(phpversion(),'5.3') >= 0) {
			$refs = array();
			foreach($arr as $key => $value) $refs[$key] = &$arr[$key];
			return $refs;
		}
		return $arr;
	}
}
