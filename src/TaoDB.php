<?php

namespace Technican\API;

class TaoDB {
    private $mysqli;
    private $dbh;
    private $config;

    function __construct($config) {
	$this->config = $config;
	if ($config['use_pdo']) {
	    $connection = "mysql:host=".$config['db_host'].";dbname=".$config['db'];
	    $this->dbh = new \PDO($connection, $config['db_user'], $config['db_pass']);
	}

	$this->mysqli = new \mysqli($config['db_host'],
	    $config['db_user'],
	    $config['db_pass'],
	    $config['db']);
    }

    function __destruct() {
	$this->mysqli->close();
    }

    public function begin_transaction() { $this->mysqli->begin_transaction(); }
    public function commit() { $this->mysqli->commit(); }
    public function rollback() { $this->mysqli->rollback(); }

    public function getDB() { return $this->mysqli; }
    public function getDBH() { return $this->dbh; }
};
