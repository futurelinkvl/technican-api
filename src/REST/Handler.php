<?php

declare(strict_types=1);

namespace Technican\API\REST;

use Technican\API\TaoDB;

class Handler {
	private $args;
	private $db;

	function __construct(TaoDB $db) {
		$this->db = $db;
	}

	function process(&$status) {
		$status = "Unimplemented";
		return false;
	}

	function getDB() { return $this->db; }
	function getDBH() { return $this->db->getDB(); }
	function setArgs($args) { $this->args = $args; }
	function getArg($index) { return $this->args[$index]; }

	function getToken() { return isset($_SERVER['HTTP_AUTHORIZATION']) ? $_SERVER['HTTP_AUTHORIZATION'] : NULL; }
}
